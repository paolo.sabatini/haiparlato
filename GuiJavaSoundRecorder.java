import javax.swing.*;    
import javax.swing.JButton;   
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent; 
 
public class GuiJavaSoundRecorder {
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
	static JavaSoundRecorder registratore= new JavaSoundRecorder();
        
         
    //private static JavaSoundRecorder recorder = new JavaSoundRecorder();
    private static void createAndShowGUI() {


        registratore.start();

        //Create and set up the window.
        JFrame frame = new JFrame("HelloWorldSwing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel pannello = new JPanel();

        //Add the ubiquitous "Hello World" label.
        JLabel label = new JLabel("Hello World");
        pannello.add(label);
        
        JButton b1 = new JButton("Cliccami !!");
		pannello.add(b1);
		frame.setContentPane( pannello );
		
        //Display the window.
        frame.pack();
        frame.setVisible(true);

         
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
				registratore.finish();
                System.out.println("WindowClosingDemo.windowClosing");
                System.exit(0);
            }
        });
    }
 
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
 
         javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
        
        
    }
}
